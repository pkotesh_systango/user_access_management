require_relative 'action_strategy.rb'
require_relative 'output_writer.rb'

class InputUser
	def initialize(name, password, actions)
		@name, @password, @actions = name, password, actions
		@user_status = false
		@password_status = false
	end

	def validate
		action_strategy = :: ActionStrategy.new
		action_strategy.implement(self)
		OutputWriter.write_to_console(self)
	end

	def add_validation_status_message(message)
		validation_status << message
	end

	def validation_status
		@validation_status ||= []
	end

	def set_user_status(flag)
		@user_status = flag
	end
	def set_password_status(flag)
		@password_status = flag
	end

	def get_name
		@name
	end
	def get_password
		@password
	end
	def get_actions
		@actions
	end
	def get_user_status
		@user_status
	end
	def get_password_status
		@password_status
	end
	def get_validation_status
		@validation_status
	end
	
end