require 'singleton'

class UserContainer
	include Singleton

	def user_array
		@user_array ||= []
	end

	def add_user(user)
		user_array << user
	end

	def get_user_array
		user_array
	end
end