require 'csv'
require_relative 'user_access_validator.rb'
require_relative 'input_constants.rb'

user_access_validator =  UserAccessValidator.new(InputConstants::USERDATA_FILENAME, InputConstants::INPUT_FILENAME)
user_access_validator.process