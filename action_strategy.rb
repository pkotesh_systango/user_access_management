require_relative 'user_container.rb'

class ActionStrategy
	def implement(input_user)
		@user_container = UserContainer.instance
		check_user_actions(input_user, @user_container)
	end

	def check_user_actions(input_user,user_container)
		user_container.get_user_array.each do |user|
			if user.get_name == input_user.get_name
				input_user.set_user_status(true)
				if input_user.get_password == user.get_password
					input_user.set_password_status(true)
				end
				input_user.get_actions.each do |action|
					if (user.get_actions_permitted.include?action) then
						input_user.add_validation_status_message("#{input_user.get_name} has #{action} access")
					else
						input_user.add_validation_status_message("#{input_user.get_name} doesn't have #{action} access")
					end
				end
			end
		end
	end

	private :check_user_actions
end