require_relative 'user.rb'

class UserInitializer
	def initialize(input_reader,user_container)
		@input_reader, @user_container = input_reader, user_container
		populate_users
	end	

	def populate_users
		input_array = @input_reader.read_file
		input_array.each do |row|
			user_from_row = User.new(user_name(row),user_password(row),user_actions_permitted(row))
			@user_container.add_user(user_from_row)
		end
	end

	def user_name(row)
		row[0].to_s rescue ''
	end
	def user_password(row)
		row[1].to_s rescue ''
	end
	def user_actions_permitted(row)
		return row[2].split(';') 
	end

	private :populate_users, :user_name, :user_password, :user_actions_permitted
end