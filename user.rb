class User
	def initialize(name, password, actions_permitted)
		@name, @password, @actions_permitted = name, password, actions_permitted
	end	

	def get_name
		@name
	end

	def get_password
		@password
	end

	def get_actions_permitted
		@actions_permitted
	end
end