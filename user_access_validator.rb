require_relative 'input_reader.rb'
require_relative 'user_container.rb'
require_relative 'user_initializer.rb'
require_relative 'inputuser_processor.rb'

class UserAccessValidator
	def initialize(userdata_filename, input_filename)
		@userdata_filename, @input_filename = userdata_filename, input_filename
	end

	def process
		input_reader_userdata = InputReader.new(@userdata_filename)
		user_initializer = UserInitializer.new(input_reader_userdata, UserContainer.instance)
		input_reader_inputdata = InputReader.new(@input_filename)
		input_user_processor = InputUserProcessor.new(input_reader_inputdata)
	end
end