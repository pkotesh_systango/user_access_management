require 'csv'

class InputReader
	def initialize(input_filename)
		@input_filename = input_filename
		validate_file
	end

	def validate_file
		puts "File doesn't exist" if(!File.file?(@input_filename) )
		puts "File doesn't have read access" if(!File.readable?(@input_filename))
	end

	def read_file
		return CSV.read(@input_filename, converters: :numeric)
	end

	private :validate_file
end