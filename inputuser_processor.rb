require_relative 'input_user.rb'

class InputUserProcessor
	def initialize(input_reader)
		@input_reader = input_reader
		populate_user
	end

	def populate_user
		input_array = @input_reader.read_file
		input_array.each do |row|
			inputuser_from_row = InputUser.new(user_name(row),user_password(row),user_actions(row))
			inputuser_from_row.validate
		end
	end

	def user_name(row)
		row[0].to_s rescue ''
	end
	def user_password(row)
		row[1].to_s rescue ''
	end
	def user_actions(row)
		return row[2].split(';') 
	end

	private :populate_user, :user_name, :user_actions, :user_password

end